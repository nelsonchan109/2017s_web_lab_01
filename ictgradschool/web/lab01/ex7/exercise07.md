# Herbivorous


* Cat
* Dog
* Horse
* Cow
* Polar Bear
* Sheep

# Carnivorous
* Frog
* Rat
* Hawk
* Lion
* Fox
* Capybara

# Omnivorous
* Raccoon
* Chicken
* Fennec Fox